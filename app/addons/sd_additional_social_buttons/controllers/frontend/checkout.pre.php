<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/
use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($mode == 'complete') {

    if (!empty($_REQUEST['order_id'])) {
    
        $order_code = db_get_field('SELECT order_code FROM ?:orders WHERE order_id = ?i', $_REQUEST['order_id']);
        if (empty($order_code)) {
            $order_code = fn_create_order_code();
            db_query('UPDATE ?:orders SET order_code = ?s WHERE order_id = ?i', $order_code, $_REQUEST['order_id']);
        }
        $params = array(
            'object' => 'products',
            'lang_code' => CART_LANGUAGE,       
        );

        $special_meta_data = fn_sd_additional_social_buttons_get_meta_data($_REQUEST['order_id']);
                        
        Tygh::$app['view']->assign('page_title', $special_meta_data['sharing_description']);

        $provider_settings = fn_sd_additional_social_buttons_get_sb_provider_settings($params);
        $url_parce = fn_url('checkout.cart?order_code='.$order_code);
        $provider_settings = fn_sd_additional_social_buttons_self_provider_settings($provider_settings, $url_parce, $special_meta_data);

        Tygh::$app['view']->assign('display_button_block', fn_sb_display_block($provider_settings));
        Tygh::$app['view']->assign('provider_settings', $provider_settings);
    }

} elseif ($mode == 'cart') {
    if (!empty($_REQUEST['order_code'])) {
        
        if (empty(Tygh::$app['session']['cart'])) {
            fn_clear_cart(Tygh::$app['session']['cart']);
        }

        $cart = &Tygh::$app['session']['cart'];

        if (empty($auth['user_id']) && Registry::get('settings.General.allow_anonymous_shopping') != 'allow_shopping') {
            return array(CONTROLLER_STATUS_REDIRECT, 'auth.login_form?return_url='.urlencode($_REQUEST['return_url']));
        }

        $order_id = db_get_field('SELECT order_id FROM ?:orders WHERE order_code = ?s', $_REQUEST['order_code']);
        $order_info = fn_get_order_info($order_id);
        $special_meta_data = fn_sd_additional_social_buttons_get_meta_data($order_id);
        $session_name = session_name();

        if (isset($_COOKIE[$session_name])) {
            $user_type_cart = ($auth['user_id'] != 0)  ? ADDITIONAL_SOC_BUT_AUTH_USER : ADDITIONAL_SOC_BUT_NOT_AUTH_USER ;

            if ($user_type_cart == ADDITIONAL_SOC_BUT_NOT_AUTH_USER) {
                $cart_products = db_get_hash_single_array('SELECT item_id,amount FROM ?:user_session_products WHERE session_id = ?s AND user_type = ?s ', array('item_id', 'amount'), $_COOKIE[$session_name], $user_type_cart);
            } elseif ($user_type_cart == ADDITIONAL_SOC_BUT_AUTH_USER) {
                $cart_products = db_get_hash_single_array('SELECT item_id,amount FROM ?:user_session_products WHERE session_id = ?s AND user_id = ?s', array('item_id', 'amount'), $_COOKIE[$session_name], $auth['user_id']);
            }
        }

        foreach ($order_info['products'] as $key_product => $product) {
            if (isset($cart_products[$key_product])) {
                if ($cart_products[$key_product] > $product['amount']) {
                    continue;
                } else {
                    $product['amount'] = $product['amount'] - $cart_products[$key_product];
                }
            }

            $product_id = $product['product_id'];
            $data_for_cart[$product_id] = array(
                'amount' => $product['amount'],
                'name' => $product['product'],
                'price' => $product['price'],
                'product_id' => $product_id,
                'product_options' => $product['extra']['product_options']
            );
            
            fn_add_product_to_cart($data_for_cart, $cart, $auth);
            unset($data_for_cart);
        }

        fn_save_cart_content($cart, $auth['user_id']);
        if (isset($cart['products'])) {
            $previous_state = md5(serialize($cart['products']));
        }
        $cart['change_cart_products'] = true;
            
        fn_calculate_cart_content($cart, $auth, 'S', true, 'F', true);       
            
        if (fn_allowed_for('ULTIMATE')) {
            $company_id = Registry::ifGet('runtime.company_id', fn_get_default_company_id());
            $site_name = fn_get_company_name($company_id);
        }
          
        $meta_data['all'] = array(
            'title' => $special_meta_data['sharing_description'],
            'url' => fn_url('checkout.cart?order_code='.$_REQUEST['order_code']),
            'image' => $special_meta_data['product_info']['image_path'],
            'image:width' => $special_meta_data['product_info']['image_x'],
            'image:height' => $special_meta_data['product_info']['image_y'],
            'site_name' => !empty($site_name) ? $site_name : Registry::get('settings.Company.company_name'),
        );
                                       
                        
        Tygh::$app['view']->assign('display_button_block', 'Y');
        Tygh::$app['view']->assign('provider_meta_data', $meta_data);
    }
}
