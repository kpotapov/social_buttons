<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Embedded;
use Tygh\Settings;
use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

function fn_sd_additional_social_buttons_get_sb_provider_settings($params)
{
    $view = Tygh::$app['view'];
    $addon_settings = Settings::instance()->getValues('social_buttons', 'ADDON');
    $self_addon_settings = Settings::instance()->getValues('sd_additional_social_buttons', 'ADDON');
    foreach ($addon_settings as $key_settings => $soc_settings) {
        if (isset($self_addon_settings['social_status'][$key_settings.'_display_on']['checkout']) && $self_addon_settings['social_status'][$key_settings.'_display_on']['checkout'] == 'Y'){ 
            $addon_settings[$key_settings][$key_settings.'_display_on']['products'] = 'Y';
        } else{
            unset($addon_settings[$key_settings][$key_settings.'_display_on']['products']);
        }    
    }
    $provider_settings = array();
    if (!empty($addon_settings)) {
        foreach ($addon_settings as $provider_name => $provider_data) {
            if (!empty($provider_data[$provider_name . '_enable']) && $provider_data[$provider_name . '_enable'] === 'Y') {
                $func_name = 'fn_' . $provider_name . '_prepare_settings';
                if (is_callable($func_name)) {
                    $provider_settings[$provider_name]['data'] = call_user_func($func_name, $provider_data, $params);
                }
                $provider_settings[$provider_name]['template'] = $provider_name . '.tpl';
                if ($view->templateExists('addons/social_buttons/meta_templates/' . $provider_name . '.tpl')) {
                    $provider_settings[$provider_name]['meta_template'] = $provider_name . '.tpl';
                }
            }
        }
    }

    return $provider_settings;
}

function fn_create_order_code($length = 10)
{
    $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $numChars = strlen($chars);
    $string = '';
    for ($i = 0; $i < $length; ++$i) {
        $string .= substr($chars, rand(1, $numChars) - 1, 1);
    }

    return $string;
}

function fn_sd_additional_social_buttons_send_order_notification(&$order_info)
{
    $special_meta_data = fn_sd_additional_social_buttons_get_meta_data($order_info['order_id']);

    $description = preg_replace('/\s/','%20', $special_meta_data['sharing_description']);
    $order_code = db_get_field('SELECT order_code FROM ?:orders WHERE order_id = ?i', $order_info['order_id']);

    if (empty($order_code)) {
        $order_code = fn_create_order_code();
        db_query('UPDATE ?:orders SET order_code = ?s WHERE order_id = ?i', $order_code, $order_info['order_id']);
    }

    $addon_settings = Settings::instance()->getValues('social_buttons', 'ADDON');
    $self_addon_settings = Settings::instance()->getValues('sd_additional_social_buttons', 'ADDON');
      
    $order_info['social_links'] = array(
        'facebook' => 'www.facebook.com/sharer.php?u='.fn_url('checkout.cart').'%26'.'order_code='.$order_code,
        'vkontakte' => 'vk.com/share.php?url='.fn_url('checkout.cart').'%26'.'order_code='.$order_code,
        'gplus' => 'plus.google.com/share?url='.fn_url('checkout.cart').'%26'.'order_code='.$order_code,
        'twitter' => 'twitter.com/share?url='.fn_url('checkout.cart').'%26'.'order_code='.$order_code.'&text='."$description",
        'pinterest' => 'www.pinterest.com/pin/create/button/?url='.fn_url('checkout.cart').'%26'.'order_code='.$order_code.'&media='.$special_meta_data['product_info']['image_path'].'&description='."$description"
    );
   
    foreach ($order_info['social_links'] as $key_link => $social_link) {
        if (!empty($addon_settings[$key_link][$key_link.'_enable']) &&  $addon_settings[$key_link][$key_link.'_enable'] === 'Y' && !empty($self_addon_settings['social_status'][$key_link.'_display_on']['mail']) && $self_addon_settings['social_status'][$key_link.'_display_on']['mail'] == 'Y') {
            continue;
        } else {
            unset($order_info['social_links'][$key_link]);
        }
    }

    return true;
}

function fn_sd_additional_social_buttons_get_meta_data($order_id)
{
    $order_info = fn_get_order_info($order_id);
    $sharing_description = Tygh\Languages\Values::getLangVar('sharing_description');
    $sharing_description = $sharing_description.':';
    foreach ($order_info['product_groups'] as $pinfo) {
        foreach ($pinfo['products'] as $info) {
            $sharing_description .= $info['product'].',';
            if (!isset($product_info)) {
                $product_info = array(
                    'image_path' => $info['main_pair']['detailed']['image_path'],
                    'image_x' => $info['main_pair']['detailed']['image_x'] ,
                    'image_y' => $info['main_pair']['detailed']['image_y']
                );
            }                                
        }
    }
    $sharing_description = preg_replace('/,$/', '.', $sharing_description);

    return array('sharing_description' => $sharing_description , 'product_info' => $product_info);

}

function fn_sd_additional_social_buttons_self_provider_settings($provider_settings, $url_parce, $special_meta_data)
{
    if (!empty($provider_settings['pinterest']['data'])) {
        $provider_settings['pinterest']['data']['url'] = urlencode($url_parce); 
        $provider_settings['pinterest']['data']['media'] = urlencode($special_meta_data['product_info']['image_path']);
        $provider_settings['pinterest']['data']['description'] = urlencode($special_meta_data['sharing_description']);
    }
    if (!empty($provider_settings['gplus']['data'])) {
        $provider_settings['gplus']['data'] .= 'data-href'.'="'.$url_parce.'" ';
    }
    if (!empty($provider_settings['facebook']['data'])) {
        $provider_settings['facebook']['data'] = preg_replace('/(?<=data-href=)\S+/','"'.$url_parce.'"',$provider_settings['facebook']['data']);
    }
    if(!empty($provider_settings['twitter']['data'])) {
        $provider_settings['twitter']['data'] = preg_replace('/(?<=data-url=)\S+/','"'.$url_parce.'"',$provider_settings['twitter']['data']);       
    }
    if(!empty($provider_settings['vkontakte']['data'])){
        $provider_settings['vkontakte']['data'] = preg_replace(array('/(?<=pageUrl: )\'\S+\'/','/&/'),array('\''.$url_parce.'\'','%26'),$provider_settings['vkontakte']['data']);
        $preg_pattern_order = PREG_PATTERN_ORDER;
        preg_match_all('/(?<=pageUrl: )\'\S+\'/',$provider_settings['vkontakte']['data'],$provider_settings['vkontakte']['data_url'],$preg_pattern_order);
        $provider_settings['vkontakte']['data_url'] = (empty($provider_settings['vkontakte']['data_url'][0][0])) ? '' : $provider_settings['vkontakte']['data_url'][0][0];    
    }
    return $provider_settings;
}
