{if $order_info.social_links}
    <div align="center" style="padding-top:20px;"> 
        {foreach from=$order_info.social_links item="social_link" key="name_social"}
            <a href="http://{$social_link}" style="color:white;">
                <div style="border-radius:100%; padding:5px; display:inline-block; margin-right:10px; border:2px solid black;">
                    <img src="{$images_dir}/addons/sd_additional_social_buttons/{$name_social}.png" height="35" width="35" >
                </div>
            </a>
        {/foreach}
    </div>
{/if}