# README #

### Module "Additional social buttons" ###

* Adds social buttons on the page completed order page and on mail

### Installation ###

* Download archive
* Add module by module manager(CSCART)
* In the settings module to add a button that you want to see on the completed order page or on mail

### Developer ###

* [Konstantin Potapov](https://bitbucket.org/kpotapov/)